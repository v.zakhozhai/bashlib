# Common records variables and functions
#
# SCRIPT_PATH="${0%\/*}"
# SCRIPT_NAME="$(basename $0)"
#
# if [ $SCRIPT_PATH == $SCRIPT_NAME ]; then
#     SCRIPT_PATH=""
# else
#     SCRIPT_PATH="${SCRIPT_PATH}/"
# fi
# . ${SCRIPT_PATH}verwalter/vw_deploy_func.sh
 
#. log.sh

RECORDS_LIST="/tmp/asterisk-records.txt"
LOCKFILE="/tmp/$(basename $0).run"

lock_set() {
    local LOCK=${1:-$LOCKFILE}

    if [ -f "$LOCK" ]; then
        log_warn "WARNING: Another instance of the script is running. Lock file: $LOCKFILE"
        exit 1
    fi

    trap "{ rm -f $LOCKFILE ; exit 255; }" EXIT

    touch $LOCKFILE
    echo $$ > $LOCKFILE
}

is_wav() {
    [ "${1##*\.}" == "wav" ] && echo "true" || echo "false"
}

# ${STRFTIME(${EPOCH},,%Y%m%d%H%M)}-OUT-${CALLERID(number)}-${EXTEN}-${UNIQUEID}.wav
# copy_record(SRC_FILE, DST_ROOT)
copy_record() {
    local RECORD_FILE="${1##*\/}"
    local SRC_PATH=$1

    local DIRECTION=$(echo $RECORD_FILE | cut -d '-' -f2)
    local CALLERID=$(echo $RECORD_FILE | cut -d '-' -f3)
    local EXTEN=$(echo $RECORD_FILE | cut -d '-' -f4)
    local UNIQUEID=$(echo $RECORD_FILE | cut -d '-' -f5)

    case $DIRECTION in
        IN)
            AGENT=$EXTEN
            ;;
        OUT)
            AGENT=$CALLERID
            ;;
        *)
            log_err "Unknown direction $DIRECTION."
            exit 1
    esac

    local DST_PATH="$2/$AGENT/$DIRECTION"
    mkdir -p $DST_PATH

    if [ -f "$DST_PATH/$RECORD_FILE" ]; then
        log_warn "Destination file $DST_PATH/$RECORD_FILE exists."
    else
        log_info "Copy $SRC_PATH to $DST_PATH/$RECORD_FILE"
        cp "$SRC_PATH" "$DST_PATH/$RECORD_FILE"
    fi
}

check_mountpoint() {
    mount | grep "$1" &> /dev/null
    if [ $? -ne 0 ]; then
        log_error "$1 mountpoint not mounted."
        mount "$1" && log_warn "$1 mountpoint mounted successfuly" || { log_error "Errors occured during mounting $1" && exit 1; }
    fi
}

