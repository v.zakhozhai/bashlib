QUEUES_ROOT="/tmp"
QUEUE_NAME="default"

# queue_init initializes queue
# queue_init(QUEUE_NAME)
queue_init() {
    local QUEUE_NAME=${1:-$QUEUE_NAME}
    [ ! -d "${QUEUES_ROOT}/${QUEUE_NAME}" ] && mkdir -p "${QUEUES_ROOT}/${QUEUE_NAME}"
}

# queue_lock_key sets lock on specified key if it exists
# queue_lock_key(KEY_NAME, QUEUE_NAME)
queue_lock_key() {
    local KEY_NAME="$1"
    local QUEUE_NAME=${2:-$QUEUE_NAME}
    if [ -f "${QUEUES_ROOT}/${QUEUE_NAME}/${KEY_NAME}" ]; then
        if [ -f "${QUEUES_ROOT}/${QUEUE_NAME}/${KEY_NAME}.lock" ]; then
	    if [ $(cat ${QUEUES_ROOT}/${QUEUE_NAME}/${KEY_NAME}.lock) -ne $$ ]; then
                log_warn "Key $KEY_NAME already in use: locked."
            fi
        else
            echo $$ > "${QUEUES_ROOT}/${QUEUE_NAME}/${KEY_NAME}.lock"
        fi
    else
        log_warn "Key $KEY_NAME does not exist."
    fi
}

# queue_release_key removes lock on specified key
# queue_release_key(KEY_NAME, QUEUE_NAME)
queue_release_key() {
    local KEY_NAME="$1"
    local QUEUE_NAME=${2:-$QUEUE_NAME}
    [ -f "${QUEUES_ROOT}/${QUEUE_NAME}/${KEY_NAME}.lock" ] && rm "${QUEUES_ROOT}/${QUEUE_NAME}/${KEY_NAME}.lock"
}

# queue_put_key creates key file
# queue_put_key(KEY_NAME, QUEUE_NAME, KEY_VAL)
queue_put_key() {
    local KEY_NAME="$1"
    local QUEUE_NAME="${2:-$QUEUE_NAME}"
    local KEY_VAL="$3"

    queue_init "$QUEUE_NAME"

    if [ -f "${QUEUES_ROOT}/${QUEUE_NAME}/${KEY_NAME}" ]; then
        log_warn "Key $KEY_NAME already exists."
    else
        echo "$KEY_VAL" > ${QUEUES_ROOT}/${QUEUE_NAME}/${KEY_NAME}
    fi
}

# queue_get_keys gets the list of queue keys
# queue_get_keys(QUEUE_NAME, KEYS_COUNT)
queue_get_keys() {
    local QUEUE_NAME=${1:-$QUEUE_NAME}
    local KEYS_COUNT=$2

    if [ -z $KEYS_COUNT ]; then
        queue_get_free_keys $QUEUE_NAME "$(ls -1t ${QUEUES_ROOT}/${QUEUE_NAME} | grep -vE '.lock$')"
    else
        queue_get_free_keys $QUEUE_NAME "$(ls -1t ${QUEUES_ROOT}/${QUEUE_NAME} | grep -vE '.lock$' | head -n $KEYS_COUNT)"
    fi
}

queue_get_free_keys() {
    local QUEUE_NAME=${1:-$QUEUE_NAME}
    local KEYS="$2"

    for KEY in ${KEYS[@]}; do
        [ -f "${QUEUES_ROOT}/${QUEUE_NAME}/${KEY}.lock" ] || echo "$KEY"
    done
}

# queue_get_key gets specified key by name
# queue_get_key(KEY_NAME, QUEUE_NAME)
queue_get_key() {
    local KEY_NAME="$1"
    local QUEUE_NAME="${2:-$QUEUE_NAME}"

    if [ ! -f "${QUEUES_ROOT}/${QUEUE_NAME}/${KEY_NAME}" ]; then
        log_warn "Key $KEY_NAME (${QUEUES_ROOT}/${QUEUE_NAME}/${KEY_NAME}) does not exist."
        echo ""
    else
        queue_lock_key "$KEY_NAME"
        echo "$(cat ${QUEUES_ROOT}/${QUEUE_NAME}/${KEY_NAME})"
    fi
}

queue_delete_key() {
    local KEY_NAME="$1"
    local QUEUE_NAME=${2:-$QUEUE_NAME}

    queue_lock_key "$KEY_NAME" "$QUEUE_NAME"
    [ -f "${QUEUES_ROOT}/${QUEUE_NAME}/${KEY_NAME}" ] && rm "${QUEUES_ROOT}/${QUEUE_NAME}/${KEY_NAME}"
    queue_release_key "$KEY_NAME" "$QUEUE_NAME"
}

