LOG_FILE=${LOG_FILE:-"/var/log/$(basename $0).log"}
# Numeric value of log level:
#   0 EMERGENCY
#   1 ALERT
#   2 CRITICAL
#   3 ERROR
#   4 WARNING
#   5 NOTICE
#   6 INFO
#   7 DEBUG
LOG_LEVEL=${LOG_LEVEL:-6}

log_level_to_str() {
    case $LOG_LEVEL in
        0)
            LOG_LEVEL_STR="EMERGENCY"
            ;;
        1)
            LOG_LEVEL_STR="ALERT"
            ;;
        2)
            LOG_LEVEL_STR="CRITICAL"
            ;;
        3)
            LOG_LEVEL_STR="ERROR"
            ;;
        4)
            LOG_LEVEL_STR="WARNING"
            ;;
        5)
            LOG_LEVEL_STR="NOTICE"
            ;;
        6)
            LOG_LEVEL_STR="INFO"
            ;;
        7)
            LOG_LEVEL_STR="DEBUG"
            ;;
        *)
            LOG_LEVEL_STR="Unknown log level $1"
    esac

    echo "$LOG_LEVEL_STR"
}

log_level_to_num() {
    case "$1" in
        [0-7])
            LOG_LEVEL_NUM=$1
            ;;
        EMERGENCY)
            LOG_LEVEL_NUM=0
            ;;
        ALERT)
            LOG_LEVEL_NUM=1
            ;;
        CRITICAL)
            LOG_LEVEL_NUM=2
            ;;
        ERROR)
            LOG_LEVEL_NUM=3
            ;;
        WARNING)
            LOG_LEVEL_NUM=4
            ;;
        NOTICE)
            LOG_LEVEL_NUM=5
            ;;
        INFO)
            LOG_LEVEL_NUM=6
            ;;
        DEBUG)
            LOG_LEVEL_NUM=7
            ;;
        *)
            LOG_LEVEL_NUM=LOG_LEVEL
    esac

    echo $LOG_LEVEL_NUM
}

# Initialize logs output
# $1 - log filename
# $2 - log level
log_init() {
    #[ -e /dev/fd/3 ] && echo "Exists"

    [ -z "$1" ] || LOG_FILE="$1"
    exec 3>&1 1>>${LOG_FILE} 2>&1

    LOG_LEVEL=$(log_level_to_num $2)

    # FYI
    #echo "This is stdout"
    #echo "This is stderr" 1>&2
    #echo "This is the console (fd 3)" 1>&3
    #echo "This is both the log and the console" | tee /dev/fd/3
}

log_message() {
    local MSG="$1"
    local LEVEL="$2"
    local CUR_DATE=$(date +"%Y-%m-%d %H:%M:%S")

    echo "$CUR_DATE $LEVEL $MSG" | tee /dev/fd/3
}

log_err() {
    log_message "$1" "ERROR"
}

log_warn() {
    log_message "$1" "WARNING"
}

log_info() {
    log_message "$1" "INFO"
}

log_debug() {
    log_message "$1" "DEBUG"
}
